extends Node

var wsc = WebSocketClient.new()

func _ready() -> void:
	var headers = PoolStringArray()
	
	if OS.get_name() != "HTML5":
		headers.append("User-Agent:GodotEngine/3.2.2")
	
	wsc.connect_to_url("ws://localhost:3000/ws", PoolStringArray(), false, headers)
	
	wsc.connect("connection_established", self, "_on_ws_connection_established")
	wsc.connect("connection_error", self, "_on_ws_connection_error")
	wsc.connect("connection_closed", self, "_on_ws_connecition_closed")
	wsc.connect("server_close_request", self, "_on_ws_server_close_request")
	wsc.connect("data_received", self, "_on_ws_data_received")
	
	set_process(true)


func _process(delta: float) -> void:
	if is_wsc_connected() or is_wsc_connecting():
		wsc.poll()



func _on_ws_connection_established(protocol: String) -> void:
	print("Connected to websocket server!")
	send_message("Hello from Godot!")


func _on_ws_connection_error() -> void:
	print("Connection error!")


func _on_ws_connecition_closed(was_clean: bool) -> void:
	print("Connection closed")


func _on_ws_server_close_request(code :int, reason: String) -> void:
	print("Close request. Code: %d, reason %s" % [code, reason])


func _on_ws_data_received() -> void:
	var message = wsc.get_peer(1).get_packet()
	if wsc.get_peer(1).was_string_packet():
		parse_message(message.get_string_from_utf8())
	else:
		print("Received bytes. size: %d" %message.size())



func is_wsc_connected() -> bool:
	return wsc.get_connection_status() == WebSocketClient.CONNECTION_CONNECTED

func is_wsc_connecting() -> bool:
	return wsc.get_connection_status() == WebSocketClient.CONNECTION_CONNECTING




func parse_message(message) -> void:
	print (message)


func send_message(message: String) -> void:
	if not is_wsc_connected():
		print("WebSocket is not connected!")
		return
	
	print("Sending message: ", message)
	wsc.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
	wsc.get_peer(1).put_packet(message.to_utf8())


func send_message_bytes(message: PoolByteArray) -> void:
	if not is_wsc_connected():
		print("WebSocket is not connected!")
		return
	
	print("Sendind binary message")
	wsc.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_BINARY)
	wsc.get_peer(1).put_packet(message)



func _on_ButtonSendText_pressed() -> void:
	# Converts a dictionary to a JSON string
	send_message(JSON.print({
		"type": "message",
		"message": "Hello!"
	}))


func _on_ButtonSendBinary_pressed() -> void:
	# Creates a buffer and add floats to it
	var b = StreamPeerBuffer.new()
	var velocity: Vector3 = Vector3(5.04, 0.04, 1.4206)
	b.put_float(velocity.x)
	b.put_float(velocity.y)
	b.put_float(velocity.z)
	send_message_bytes(b.data_array)



